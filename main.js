const {app,BrowserWindow,ipcMain, dialog} = require('electron')


let win = null
function createWindow() {
  // create window
    win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })
  win.loadFile('index.html')
  
}

// wait app ok to create window
app.whenReady().then(createWindow)

ipcMain.on('test',function (event, arg) {
  win.webContents.openDevTools()
  dialog.showErrorBox('Error box', 'salutation')
  event.returnValue = "e"
})

