const fs = require('fs')

const submit = document.getElementById('createFile')
submit.addEventListener('click', function () {
    const value = document.getElementById("text").value
    fs.writeFile("test.txt", value, (err) => {
        if (err) {
          console.log("An error ocurred creating the file " + err.message)
        }
    
        console.log("The file has been succesfully saved");
      });
})